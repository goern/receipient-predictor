#!/bin/bash

touch centos-devel.mbox

for y in 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017
do
    for m in January February March April May June July August September October November December
    do
        curl -gO https://lists.centos.org/pipermail/centos-devel/$y-$m.txt.gz
    done
done

rm 2005-February.txt.gz 2005-January.txt.gz # they are outliers :)
gunzip 20*.txt.gz

cat 20*.txt >centos-devel.mbox
rm 20*.txt
