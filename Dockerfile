FROM registry.fedoraproject.org/fedora:27

ENV NAME=receipient-predictor VERSION=0.1.0 RELEASE=1 ARCH=x86_64 LANG=C

LABEL   com.redhat.component="$NAME" \
        name="$FGC/$NAME" \
        version="$VERSION" \
        release="$RELEASE.$DISTTAG" \
        architecture="$ARCH" \
        summary="receipient-predictor will generate a receipient-predictor model." \
        maintainer="goern <goern@b4mad.net>"

WORKDIR /opt/fedora/receipient-predictor

COPY . /opt/fedora/receipient-predictor

RUN dnf install -y which && \
    pip3 install pipenv && \
    cd /opt/fedora/receipient-predictor && \
    pipenv install --three

CMD /opt/fedora/receipient-predictor/run-all-tasks.sh