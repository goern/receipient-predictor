
# coding: utf-8

# # Receipient Predictor
# 
# This is an experiment to:
# 
#  * Reading Inputs: extract topic from mails
#  * Preparing data: map a set of topics to a person's email
#  * predict a list of emails based on an input mail draft
# 
# ## Assumptions
# 
# We will use Python3.5, it is required to use Tensorflow within a Jupyter notebook. All required python modules are managed using [pipenv](https://docs.pipenv.org/install.html#using-installed-packages).
# 
# We use english language.

# In[1]:


import tensorflow as tf
import pandas as pd
import numpy as np
import sklearn
import scipy
import keras
import sys

print("You are using Tensorflow version {}".format(tf.__version__))
print('Keras version ' + keras.__version__)
print('sklearn version ' + sklearn.__version__)
print('scipy version ' + scipy.__version__)
print('Pandas version: ' + pd.__version__)
print('NumPy version: ' + np.__version__)
print('Python version ' + sys.version)


# # Reading Inputs
# The first thing to do is to read a lot of mail. Just as us humans, AI needs to learn by reading mails and extract the relevant topics. We will be using the [http://www.nltk.org/](NLTK) to tokenize and analyse the inputs.
# 
# When you are running the next cells, you migh see that NLTK is downloading packages from the Internet.

# In[ ]:


from nltk.tokenize import WhitespaceTokenizer
from nltk.util import ngrams
from nltk.corpus import stopwords
import nltk

nltk.download('stopwords')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')


# For now we use 'an email' (INPUT_HEADERS and INPUT_PAYLOAD), this needs to be extended to read from a mbox or ...

# In[ ]:


from email.parser import Parser

INPUT_HEADERS = """From: Marcel Hild <mhild@redhat.com>
To: artificial-dunno <artificial-dunno@redhat.com>
Date: Dec 9, 2017, 11:07 AM
"""

INPUT_PAYLOAD = """Hey all,
I want to get my hands dirty on ML and came up with this pet project: archives
Output: Suggest E-Mail recipients based 
Input: Subject/Body of draft archives
Data: mailinglist archives
I archives have basic knowledge about statistical learning [1] and my background is more of a solid development/systems engineer.
The goal is to get me started on the state of the art tooling used in ML.
Do you have suggestions on the tooling and models/algorithms I should look into?
Thanks for any pointers -

[1] http://www-bcf.usc.edu/~gareth/ISL/
"""


# So the first thing to do is to split the input up into sentences and remove all the english stopwords. We will also do 'part of speech tagging' ([POS](https://en.wikipedia.org/wiki/Part-of-speech_tagging)). As NLTK used Penn Treebank Tags, it might be good to have a look at [list of tags](https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html).

# We will define two functions that tokenize and tag `tokenize_and_tag()` and extract all the nouns `get_nouns()` from a text/email body. We suspect the nouns to be good candidates for topics.

# In[ ]:


def tokenize_and_tag(body):
    stopset = set(stopwords.words('english'))
    stopset.update([']', ']'])
    stopset.update(['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'])
    stopset.update(['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'])

    # FIXME [ ] seem to be ignored
    
    input = ' '.join([i for i in body.split() if i.strip().lower() not in stopset and i.isalpha()])
    sentences = nltk.sent_tokenize(input)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences_pos_tagged = [nltk.pos_tag(sent) for sent in sentences]
    
    return sentences, sentences_pos_tagged


# In[ ]:


def get_nouns(pos_tagged_body):
    names = set()
    nouns = set()

    for tagged_sentence in pos_tagged_body:
        for chunk in nltk.ne_chunk(tagged_sentence):
            if type(chunk) == nltk.tree.Tree:
                if chunk.label() == 'PERSON':
                    # This might be a person...
                    names.add(' '.join([c[0] for c in chunk]))
            else:
                if chunk[1] in ['NN', 'NNS', 'NNP', 'NNPS']:
                    # This might be a topic...
                    nouns.add(chunk[0])

    return nouns, names


# Applying both functions to out initial payload...

# In[ ]:


sentences, sentences_pos_tagged = tokenize_and_tag(INPUT_PAYLOAD)
nouns, names = get_nouns(sentences_pos_tagged)

for noun in nouns:
    print(noun)


# The resulting list of nouns seems to contain things we can get rid of: '\[' 'http' ... we should clean up the result a little bit more. But for the beginning this is a sufficient result.

# # Preparing data
# 
# Let's see if we can apply this to a mail from an archive. We provide and archive of centos-devel which can be used for experiments.
# 
# From each mail we will get the nouns, and store these with the mail's author to a Pandas data structure.
# 
# **HINT:** there might be error about string being unconvertible to Timestamp, which is simply ignore. We will loose a few mails due to this, but it is of no relevance.

# In[ ]:


import mailbox
import os

mbox = mailbox.mbox('centos-devel.mbox')
extracted_data = pd.DataFrame({'author': 'goern@b4mad.net', 
                               'date': pd.Timestamp('19740201'), 
                               'nouns': pd.Series(['b4mad', 'science'], name='nouns')})

for id, msg in mbox.iteritems():   
    if not msg.is_multipart():
        # FIXME we should use .walk()
        body = msg.get_payload()
        
        # lets clean up quoted parts of the email body
        # TODO this could throw
        body = os.linesep.join([s for s in body.splitlines() if not s.startswith('>')])
        
        sentences, sentences_pos_tagged = tokenize_and_tag(body)
        nouns, names = get_nouns(sentences_pos_tagged)

        try:
            extracted_data = pd.concat([extracted_data, pd.DataFrame({'author': msg.get_from().replace(' at ', '@').split(' ', 1)[0], 
                               'date': pd.Timestamp(msg.get('Date')), 
                               'nouns': pd.Series(list(nouns), name='nouns')})])
        except ValueError as ve:
            print("{}: {}: {}".format(ve,msg.get('Message-ID'),msg.get('Date')))
            continue
            
extracted_data.to_csv('extracted_data.csv',index=False,header=True)

print("extracted_data info:\n{}\n".format(extracted_data.info()))
print("Description of extracted_data:\n{}".format(extracted_data.describe()))

