# Receipient Predictor

This is a Jupyter notebook, the environment includes Tensorflow and Tensorflowboard.

## Usage

Install the dependencies using `pipenv install`, make sure you are using python3.5 as this is the version supported by Tensorflow 1.4

Now you are ready to start the Jupyter notebook: `pipenv shell ; jupyter notebook` ...

Have Fun.

# Copyright

bn-bruecken
Copyright (C) 2017  Christoph Görn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Attribution

The `Predict_Logo` is from https://commons.wikimedia.org/wiki/File:Predict_Logo.jpg